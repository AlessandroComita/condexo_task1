Il nome del database deve essere 
    condexo_test1

Tra i file del progetto, c'è un file sql «utenti.sql» che contiene il database «utenti», utilizzato dalla webapp: esso è precaricato con una dozzina di record per poter testare la webapp.

La porta per il servizio MySQL su cui è impostato il progetto è
    3307

Se si vuole cambiare questa impostazione nel progetto, bisogna agire sul file «/components/funzioni.php»: la prima funzione ivi presente è quella che stabilisce la connessione, e ha la porta come variabile: basta inserire qui la porta standard 3306 come valore della variabile «$port» oppure togliere completamente la variabile, riga 13 e riga 15, cosicché la connessione venga stabilita sulla porta standard 3306.

Il progetto è realizzato tramite XAMPP, quindi per far girare la wep app sul server in locale, la cartella del progetto, «condexo_task1», deve essere posizionata nella cartella relativa al server di XAMPP, che di default è
    C:\xampp\htdocs

Cosicché, una volta avviato XAMPP e relativi servizi Apache e MySQL, la webapp partirà inserendo nel browser l'indirizzo 
http://localhost/condexo_task1/

A questo indirizzo si aprirà la pagina «index.php», che è quella che contiene la form per la registrazione (creazione di un nuovo utente).

La webapp ha anche una sezione dedicata all'admin, non raggiungibile dalla pagina di registrazione; per accedervi bisogna inserire nel browser l'indirizzo:
http://localhost/condexo_task1/admin.php


