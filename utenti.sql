-- phpMyAdmin SQL Dump
-- version 5.2.0
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1:3307
-- Generation Time: Jul 08, 2022 at 08:08 PM
-- Server version: 10.4.24-MariaDB
-- PHP Version: 8.1.6

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `condexo_test1`
--

-- --------------------------------------------------------

--
-- Table structure for table `utenti`
--

CREATE TABLE `utenti` (
  `id_utente` int(3) NOT NULL,
  `nome_utente` varchar(50) NOT NULL,
  `cognome_utente` varchar(50) NOT NULL,
  `email_utente` varchar(255) NOT NULL,
  `password_utente` varchar(255) NOT NULL DEFAULT '123456789',
  `data_nascita_utente` date DEFAULT NULL,
  `telefono_utente` varchar(50) DEFAULT NULL,
  `indirizzo_utente` varchar(255) DEFAULT NULL,
  `civico_utente` varchar(5) DEFAULT NULL,
  `cap_utente` varchar(6) DEFAULT NULL,
  `citta_utente` varchar(50) DEFAULT NULL,
  `nazionalita_utente` varchar(20) DEFAULT NULL,
  `is_admin` tinyint(1) NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `utenti`
--

INSERT INTO `utenti` (`id_utente`, `nome_utente`, `cognome_utente`, `email_utente`, `password_utente`, `data_nascita_utente`, `telefono_utente`, `indirizzo_utente`, `civico_utente`, `cap_utente`, `citta_utente`, `nazionalita_utente`, `is_admin`) VALUES
(1, 'Mario', 'Merola', 'mariomerola@email.it', 'asdasdasd', '1941-01-01', '3211122333', 'Piazza Plebiscito', '33', '91921', 'Roma', 'italiana', 0),
(3, 'Davide', 'Pisani', 'davidepisani@email.it', 'asdasdasd', '1912-12-12', '3881188999', 'Via dei Rovi', '22', '00010', 'Roma', 'italiana', 0),
(4, 'Filippo', 'Guidi', 'filippoguidi@hotmail.com', '123123123', '1913-12-13', '3119087666', 'Via Roma', '22', '14010', 'Mantova', 'italiana', 0),
(5, 'Gianna', 'Giannini', 'giannagiannini@emailfacile.it', '123123123', '1977-07-07', '3991998766', 'Via Soia', '22/b', '21010', 'Varese', 'italiana', 0),
(6, 'Jassim', 'Asad', 'jassimasad@marocco.it', 'asdasdasd', '1981-09-09', '3118877666', 'Via CasaBlanca', '88', '31010', 'Treviso', 'marocchina', 0),
(7, 'Avagyan', 'Barsamian', 'barsamian@yahoo.com', 'asdadsasd', '1955-05-05', '3990011222', 'Via della Libertà', '33', '15010', 'Alessandria', 'armena', 0),
(8, 'Luca', 'De Mauro', 'lucademauro@hotmail.it', '123123123', '1980-08-08', '3219900111', 'Piazza Genova', '91', '43120', 'Parma', 'italiana', 0),
(9, 'Lyudmila', 'Stepanova', 'stepanova@email.ru', '888999888', '1920-12-12', '3907878666', 'Via Tetris', '11', '70010', 'Bari', 'russa', 0),
(10, 'Maria', 'Rebecchi', 'rebecchi@email.it', 'rebecchirebecchi', '1960-09-09', '3812211222', 'Via Molle', '44', '64010', 'Teramo', 'italiana', 0),
(11, 'Davide', 'Molli', 'molli@hotmail.it', '132123123', '1995-05-05', '3991100222', 'Corso Mazzini', '83', '56010', 'Pisa', 'italiana', 0),
(12, 'Martina', 'Colombari', 'colombari@gmail.com', '123123123', '1988-12-08', '3019922111', 'Vico degli Struzzi', '41', '95010', 'Catania', 'italiana', 0),
(262, 'Gianni', 'Boncompagni', 'gianni@mediaset.it', 'italia1', '1941-12-12', '3912121211', 'Via Teulada', '12', '12999', 'Roma', 'italiana', 0),
(263, 'Phil', 'Anselmo', 'pantera@down.it', 'sickness', '1971-12-09', '3112195666', 'Via Bay Area', '99', '77111', 'Los Angeles', 'americana', 1),
(264, 'Tommaso', 'Marinetti', 'marinetti@futurismo.it', '123123123', '1875-12-12', '3159988777', 'Via degli Abruzi', '11', '10291', 'Milano', 'italiana', 0),
(265, 'Mirko', 'De Federici', 'defederici@yahoo.com', '123123123123', '1995-02-07', '', 'Piazza Benedetti', '33', '90100', 'Pisa', 'italiana', 0),
(266, 'Gianluca', 'Raggi', 'gianlucaraggi@email.it', '123123123123', '1988-12-11', '3119900212', 'Via dei Vichi Ciechi', '28', '87122', 'Messina', 'italiana', 0),
(267, 'Ambra', 'Angioini', 'ambra@nonelarai.tv', 'asdasdasdasd', '1978-11-09', '3219922321', 'Viale degli Angioini', '99', '78901', 'Mantova', 'italiana', 0),
(269, 'Paride', 'Pavido', 'pavido@pec.it', 'asdasdasd', '1999-09-21', '3889922111', '', '', '', '', '', 0),
(270, 'James', 'LaBrie', 'labrie@dream.it', '123123123123', '1973-11-09', '3291199211', 'Via Cipolle', '12', '77611', 'Boston', 'americana', 0);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `utenti`
--
ALTER TABLE `utenti`
  ADD PRIMARY KEY (`id_utente`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `utenti`
--
ALTER TABLE `utenti`
  MODIFY `id_utente` int(3) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=299;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
