<!-- sidebar -->
<button
    id="button_sidebar_toggle"
    class="position-absolute">
        <img 
            width="40"
            src="images/admin.png">
</button>

<div 
    id="admin_sidebar"
    class="sidebar-admin d-flex flex-column flex-shrink-0 
            p-3 text-white bg-dark h-100 position-md-relative " >
    <div class="d-flex justify-content-between align-items-center"
            style="height: 40px;">
    
        <button 
            class="d-flex align-items-center  mb-md-0 me-md-auto text-white text-decoration-none"
            style="background-color: transparent; border: 0px;">
            <span class="fs-4">Admin</span>
        </button>

        <button
            id="admin_button">
            <i class="fa-solid fa-eye-slash fa-xl"></i>
        </button>
    </div>
    <hr>
    <ul class="nav nav-pills flex-column mb-auto">
    <li class="nav-item">
        <a href="admin.php?content=home" class="nav-link active" aria-current="page">
        <i class="fa-solid fa-house me-2"></i>
        Home
        </a>
    </li>
    <li>
        <a href="admin.php?content=dashboard" class="nav-link text-white">
        <i class="fa-solid fa-gauge-high me-2"></i>
        Dashboard
        </a>
    </li>

    <li>
        <a href="admin.php?content=utenti" class="nav-link text-white">
        <i class="fa-solid fa-users me-2"></i>
        Utenti
        </a>
    </li>

    <li>
        <a href="index.php"
            target="_blank"
            class="nav-link text-white">
            <i class="fa-solid fa-user-plus me-2"></i>
        Registrazione
        </a>
    </li>

    </ul>
    <hr>
    <div class="dropdown">
        <a 
            href="#" class="d-flex align-items-center text-white text-decoration-none dropdown-toggle" id="dropdownUser1" data-bs-toggle="dropdown" aria-expanded="false">
            <img src="./images/admin.png" alt="" width="32" height="32" class="rounded-circle me-2">
            <strong>admin</strong>
        </a>
        <ul class="dropdown-menu dropdown-menu-dark text-small shadow" aria-labelledby="dropdownUser1">
            <li><a class="dropdown-item" href="#">New project...</a></li>
            <li><a class="dropdown-item" href="#">Settings</a></li>
            <li><a class="dropdown-item" href="#">Profile</a></li>
            <li><hr class="dropdown-divider"></li>
            <li><a class="dropdown-item" href="#">Sign out</a></li>
        </ul>
    </div>
</div>
<!-- fine sidebar -->