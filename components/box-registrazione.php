<body class="body-index" >

    <section 
        id="sezione-main"
        class="pt-5 text-white" 
        style="background-color: transparent; overflow-x: hidden !important;">

        <span 
            id="wrap-messaggio-registrazione"
            class="rounded">
                <div
                    id="box-messaggio-registrazione"
                    class="rounded text-center 
                        d-flex justify-content-center align-items-center">
                    Registrazione avvenuta con successo
                </div>
        </span>


    <div class="container">
        <div class="row d-flex justify-content-center align-items-center ">
            <div class="col-lg-12 col-xl-11 overflow-hidden ">

                <div class="card text-black box-registrazione">
                    <div class="card-body p-md-5" >
                        <form 
                            action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]);?>"
                            method="POST"
                            class="mx-1 mx-md-4">
                            <div class="row justify-content-center">

                                <p class="text-center text-white h1 fw-bold mb-5 mx-1 mx-md-4 mt-4" style="color: black">Registrazione Utente</p>

                                <!-- blocco form 1 -->
                                <div class="col-md-10 col-lg-6  ">
                                    
                                
                                    <!-- NOME -->
                                    <div class="d-flex flex-row align-items-center">
                                        
                                        <i class="d-flex align-items-center justify-content-center
                                        fas fa-user fa-lg me-3 fa-fw"></i>
                                        <div class="form-outline flex-fill mb-0">
                                            <input 
                                                name="nome"
                                                maxlength="50"
                                                minlength="2" 
                                                type="text" 
                                                id="input_nome" 
                                                class="form-control" 
                                                placeholder="Nome" 
                                                required
                                            />
                                        </div>                           
                                    </div>

                                    <!-- box messaggio validazione input -->
                                    <div 
                                        class="mb-4 pe-2 text-end"
                                        style="color: yellow">
                                        <?php 
                                            if (isset($_SESSION['ErrNome']))
                                            {
                                                echo $_SESSION['ErrNome']; 
                                            }
                                        ?>
                                    </div>
                

                                    <!-- COGNOME -->
                                    <div class="d-flex flex-row align-items-center mt-4">
                                        <i class="d-flex align-items-center justify-content-center
                                        fas fa-user fa-lg me-3 fa-fw"></i>
                                        <div class="form-outline flex-fill mb-0">
                                        <input 
                                            name="cognome" 
                                            maxlength="50"
                                            minlength="2" 
                                            type="text" 
                                            id="input_cognome" 
                                            class="form-control" 
                                            placeholder="Cognome" 
                                            required
                                        />
                                        </div>
                                    </div>

                                    <!-- box messaggio validazione input -->
                                    <div 
                                        class="mb-4 pe-2 text-end"
                                        style="color: yellow">
                                        <?php 
                                            if (isset($_SESSION['ErrCognome']))
                                            {
                                                echo $_SESSION['ErrCognome']; 
                                            }
                                        ?>
                                    </div>



                                    <!-- EMAIL -->
                                    <div class="d-flex flex-row align-items-center">
                                        <i class="fas fa-envelope fa-lg me-3 fa-fw"></i>
                                        <div class="form-outline flex-fill mb-0">
                                            <input 
                                                name="email" 
                                                maxlength="255" 
                                                type="email" 
                                                id="input_email" 
                                                class="form-control" 
                                                placeholder="Email" 
                                                required                
                                            />
                                        </div>
                                    </div>

                                    <!-- box messaggio validazione input -->
                                    <div 
                                        class="mb-4 pe-2 text-end"
                                        style="color: yellow">
                                        <?php 
                                            if (isset($_SESSION['ErrEmail']))
                                            {
                                                echo $_SESSION['ErrEmail']; 
                                            }
                                        ?>
                                    </div>



                                    
                                    <!-- PASSWORD -->
                                    <div class="d-flex flex-row align-items-center mt-4">
                                        <i class="fas fa-lock fa-lg me-3 fa-fw"></i>
                                        <div class="form-outline flex-fill mb-0">
                                            <input 
                                                name="password"
                                                minlength="8"
                                                maxlength="127"  
                                                type="password" 
                                                id="input_password" 
                                                class="form-control" 
                                                placeholder="Password" 
                                                required
                                            />
                                        </div>
                                    </div>

                                    <!-- box messaggio validazione input -->
                                    <div 
                                        class="mb-4 pe-2 text-end"
                                        style="color: yellow">
                                        <?php 
                                            if (isset($_SESSION['ErrPassword']))
                                            {
                                                echo $_SESSION['ErrPassword']; 
                                            }
                                        ?>
                                    </div>                                
                



                                    <!-- CONFERMA PASSWORD -->
                                    <div class="d-flex flex-row align-items-center mt-4">
                                        <i class="fas fa-key fa-lg me-3 fa-fw"></i>
                                        <div class="form-outline flex-fill mb-0">
                                            <input 
                                                name="conferma_password"
                                                minlength="8"
                                                maxlength="127"   
                                                type="password" 
                                                id="input_conferma_password" 
                                                class="form-control" 
                                                placeholder="Conferma Password" 
                                                required
                                            />
                                        </div>
                                    </div>

                                    <!-- box messaggio validazione input -->
                                    <div 
                                        class="mb-4 pe-2 text-end"
                                        style="color: yellow">
                                        <?php 
                                            if (isset($_SESSION['ErrConfermaPassword']))
                                            {
                                                echo $_SESSION['ErrConfermaPassword']; 
                                            }
                                        ?>
                                    </div>
                                </div>




                                <!-- secondo blocco form -->
                                <div class="col-md-10 col-lg-6 ps-3">          


                                    <!-- DATA DI NASCITA -->
                                    <div class="d-flex flex-row align-items-center">
                                        <i class="fa-regular fa-calendar-days fa-lg me-3 fa-fw"></i>
                                        <div class="d-flex flex-column flex-md-row align-items-start align-items-md-center w-100">

                                            <div class="form-outline mb-0 d-flex align-items-center flex-md-row flex-column">
                                                <input 
                                                    name="data_nascita" 
                                                    min="1850-01-01"
                                                    max="2004-01-01"
                                                    type="date" 
                                                    id="input_nascita" 
                                                    class="form-control"
                                                    placeholder="Data di nascita"
                                                    style="width: 150px;"
                                                />
                                                <label class="text-white form-label p-0 m-0 me-md-3 me-2 ms-2" for="data_nascita"><nobr>Data di nascita</nobr></label>                           
                                            </div>
                                        </div>
                                    </div>

                                    <!-- box messaggio validazione input -->
                                    <div 
                                        class="mb-4 pe-2 text-start"
                                        style="color: yellow">
                                        <?php 
                                            if (isset($_SESSION['ErrDataNascita']))
                                            {
                                                echo $_SESSION['ErrDataNascita']; 
                                            }
                                        ?>
                                    </div>
                                    

                

                                    <!-- TELEFONO -->
                                    <div class="d-flex flex-row align-items-center mt-4">
                                        <i class="fa-solid fa-square-phone fa-lg me-3 fa-fw"></i>
                                        <div class="form-outline flex-fill mb-0">
                                            <input 
                                                name="telefono"
                                                minlength="8"
                                                maxlength="50"    
                                                type="text" 
                                                id="input_telefono" 
                                                class="form-control" 
                                                placeholder="Telefono"
                                            />
                                        </div>
                                    </div>

                                    <!-- box messaggio validazione input -->
                                    <div 
                                        class="mb-4 pe-2 text-end"
                                        style="color: yellow">
                                        <?php 
                                            if (isset($_SESSION['ErrTelefono']))
                                            {
                                                echo $_SESSION['ErrTelefono']; 
                                            }
                                        ?>
                                    </div>



                
                                    <!-- INDIRIZZO -->
                                    <div class="d-flex flex-row align-items-center mt-4">
                                        <i class="fa-solid fa-road fa-lg me-3 fa-fw"></i>
                                        <div class="form-outline d-flex mb-0 w-100 flex-column flex-md-row">
                                            <input 
                                                name="indirizzo"
                                                minlength="5"
                                                maxlength="255"   
                                                type="text"
                                                id="input_indirizzo" 
                                                class="form-control" 
                                                placeholder="Indirizzo"
                                            />
                                                        
                                            <input 
                                                name="civico"
                                                maxlength="5"   
                                                type="text" 
                                                style="width: 100px;"
                                                id="input_civico" 
                                                class="form-control ms-0 ms-md-2 mt-2 mt-md-0" placeholder="Civico"
                                            />
                                        </div>
                                    </div>

                                    <!-- box messaggio validazione input -->
                                    <div 
                                        class="mb-4 pe-2 text-end"
                                        style="color: yellow">
                                        <?php 
                                            if (isset($_SESSION['ErrIndirizzo']))
                                            {
                                                echo $_SESSION['ErrIndirizzo']; 
                                            }
                                        ?>
                                    </div>
            
            
            

                                    <!-- CAP -->
                                    <div class="d-flex flex-row align-items-center mt-4">
                                        <i class="fa-solid fa-city fa-lg me-3 fa-fw"></i>
                                        <div class="form-outline d-flex mb-0 w-100 flex-column flex-md-row">                 
                                            <input 
                                                name="cap"
                                                minlength="5"
                                                maxlength="5"   
                                                type="text" 
                                                id="input_cap" 
                                                class="form-control me-2 mb-2 mb-md-0" 
                                                style="width: 100px;"
                                                placeholder="CAP"
                                            />

                                            <input 
                                                name="citta"
                                                minlength="2"
                                                maxlength="50"   
                                                type="text" 
                                                id="input_citta" 
                                                class="form-control" 
                                                placeholder="Città"
                                            />
                                        </div>
                                    </div>

                                    <!-- box messaggio validazione input -->
                                    <div 
                                        class="mb-4 pe-2 text-end"
                                        style="color: yellow">
                                        <?php 
                                            if (isset($_SESSION['ErrCitta']))
                                            {
                                                echo $_SESSION['ErrCitta']; 
                                            }
                                        ?>
                                    </div>
                    


                    
                                    <!-- NAZIONALITÀ -->
                                    <div class="d-flex flex-row align-items-center mt-4">
                                        <i class="fa-solid fa-earth-americas fa-lg me-3 fa-fw"></i>
                                        <div class="form-outline flex-fill mb-0">
                                            <input 
                                                name="nazionalita"
                                                minlength="2"
                                                maxlength="20"    
                                                type="text" 
                                                id="input_nazione" 
                                                class="form-control" 
                                                placeholder="Nazionalità" 
                                            />
                                        </div>  
                                    </div>

                                    <!-- box messaggio validazione input -->
                                    <div 
                                        class="mb-4 pe-2 text-end"
                                        style="color: yellow">
                                        <?php 
                                            if (isset($_SESSION['ErrNazionalita']))
                                            {
                                                echo $_SESSION['ErrNazionalita']; 
                                            }
                                        ?>
                                    </div>


                                <!-- chiude secondo blocco form -->
                                </div>

                        
                            <!-- chiude row interna della form -->
                            </div>


                            <div class="d-flex justify-content-center w-100  mb-3 mb-lg-4 mt-3">
                                <button 
                                    type="submit" 
                                    name="registrazione_submit"
                                    class="btn btn-lg w-100" style="background-color: #00bbaa; color: white;">
                                    Registra
                                </button>
                            </div>

                        </form>
                    

                
                    </div>
                </div>
            </div>
        </div>


    <!-- chiude container -->
    </div>

</section>

<?php 
    registra_utente(); 
    impedisci_submit_al_refresh();
?>