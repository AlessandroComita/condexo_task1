<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
  

    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">



    <!-- font awesome 5 kit  -->
    <script src="https://kit.fontawesome.com/767a97e380.js" crossorigin="anonymous"></script>

    <!-- animazioni -->
    <script src="./js/animazioni.js"></script>

    <!-- custom style -->

    <link rel="stylesheet" href="css/style.css">

    <!-- stili temporanei di appoggio - fase di sviluppo  -->
    <link rel="stylesheet" href="css/appoggio.css">


    <title>Task 1</title>
</head>