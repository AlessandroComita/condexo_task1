<?php

    $utente = dettaglio_utente();
    modifica_utente($utente['id_utente']);
?>


<div class="container">
    <div class="row">
        <div class="col-12">
            <h1 class='ms-5 mt-5'>Modifica Utente</h1>
            <hr style= "width:100%;
                    border-style: inset;
                    border-width: 5px;">
        </div>
    </div>



    <div class="row">
        <div class="col-12">

            <form 
                method="POST"
                action=""
            
                class="mx-1 mx-md-4">
                <div class="row justify-content-center">



                    <!-- blocco form 1 -->
                    <div class="col-md-10 col-lg-6  ">
                        
                    

                        <div class="d-flex flex-row align-items-center mb-4">
                            
                            <i class="d-flex align-items-center justify-content-center
                            fas fa-user fa-lg me-3 fa-fw"></i>

                            <div class="form-outline flex-fill mb-0">

                                <label class="text-black form-label p-0 m-0 me-md-3 me-2 ms-2" for="data_nascita"><nobr>Nome</nobr></label>  

                                <input name="nome" type="text" id="input_nome" class="form-control" 
                                value="<?php echo $utente['nome_utente']; ?>"
                                
                                />
                    
                            </div>   
                        </div>
                
    

                        <div class="d-flex flex-row align-items-center mb-4">
                            <i class="d-flex align-items-center justify-content-center
                            fas fa-user fa-lg me-3 fa-fw"></i>
                            
                            
                            
                            <div class="form-outline flex-fill mb-0">

                                <label class="text-black form-label p-0 m-0 me-md-3 me-2 ms-2" for="data_nascita"><nobr>Cognome</nobr></label>  
                                <input name="cognome" type="text" id="input_cognome" class="form-control" 
                                value="<?php echo $utente['cognome_utente']; ?>" 
                                
                                 />
                            </div>
                        </div>



                        <div class="d-flex flex-row align-items-center mb-4">
                        <i class="fas fa-envelope fa-lg me-3 fa-fw"></i>
                  
                        

                        
                            <div class="form-outline flex-fill mb-0">
                                <label class="text-black form-label p-0 m-0 me-md-3 me-2 ms-2" for="data_nascita"><nobr>Email</nobr></label>  

                                <input name="email" type="email" id="input_email" class="form-control" 
                                value="<?php echo $utente['email_utente']; ?>" 
                                
                                />
                            </div>
                        </div>
    
                        
                        <div class="d-flex flex-row align-items-center mb-4">
                                <i class="fas fa-lock fa-lg me-3 fa-fw"></i>

                            <div class="form-outline flex-fill mb-0">

                                <label class="text-black form-label p-0 m-0 me-md-3 me-2 ms-2" for="data_nascita"><nobr>Password</nobr></label>  
                                    <input name="password" type="password" id="input_password" class="form-control" 
                                    
                                    value="<?php echo $utente['password_utente']; ?>" 
                                    required/>

                                </div>
                                </div>



                        <div class="d-flex flex-row align-items-center mb-4">
                            <i class="fa-regular fa-calendar-days fa-lg me-3 fa-fw"></i>
                            <div class="d-flex flex-column flex-md-row align-items-start align-items-md-center w-100">

                            <div class="form-outline flex-fill mb-0">


                                <label class="text-black form-label p-0 m-0 me-md-3 me-2 ms-2" for="data_nascita"><nobr>Data di nascita</nobr></label>  

                                <input name="data_nascita" type="date" 
                                style="width: 150px;"
                                id="input_nascita" class="form-control" placeholder="Data di nascita"
                                min="1850-01-01"
                                value="<?php echo $utente['data_nascita_utente']; ?>"
                               
                                


                                />
                         
                            </div>

                            </div>
                        </div>






    
                    
    
                    </div>


                    <!-- secondo blocco form -->
                    <div class="col-md-10 col-lg-6 ps-3">
    
                        
    
                    <div class="d-flex flex-row align-items-center mb-4">
                            <i class="fa-solid fa-square-phone fa-lg me-3 fa-fw"></i>
                            <div class="form-outline flex-fill mb-0">

                                <label class="text-black form-label p-0 m-0 me-md-3 me-2 ms-2" for="data_nascita"><nobr>Telefono</nobr></label>  
                                <input name="telefono" type="text" id="input_telefono" class="form-control" 
                                value="<?php echo $utente['telefono_utente']; ?>"
                                
                                />
                            </div>
                        </div>
    

                        <div class="d-flex flex-row align-items-center mb-4">
                            <i class="fa-solid fa-road fa-lg me-3 fa-fw"></i>

                            <div class="form-outline flex-fill mb-0">

                                <label class="text-black form-label p-0 m-0 me-md-3 me-2 ms-2" for="data_nascita"><nobr>Indirizzo</nobr></label>  
                                <input name="indirizzo" type="text"
                            
                                id="input_indirizzo" class="form-control"                    
                                value="<?php echo $utente['indirizzo_utente']; ?>"
                                
                                />
                            </div>
                        </div>
                    

                        <div class="d-flex flex-row align-items-center mb-4">
                            <i class="fa-solid fa-road fa-lg me-3 fa-fw"></i>

                            <div class="form-outline flex-fill mb-0">

                            <label class="text-black form-label p-0 m-0 me-md-3 me-2 ms-2" for="data_nascita"><nobr>Numero Civico</nobr></label> 
                            <input name="civico" type="text" 
                                        style="width: 100px;"
                                    
                                        id="input_civico" class="form-control ms-0 ms-md-2 mt-2 mt-md-0" 
                                        value="<?php echo $utente['civico_utente']; ?>"
                                        
                                        />
                            </div>
                        </div>





                      



                        <div class="d-flex flex-row align-items-center mb-4">

                            <i class="fa-solid fa-city fa-lg me-3 fa-fw"></i>
                            <div class="form-outline flex-fill mb-0">               


                                <label class="text-black form-label p-0 m-0 me-md-3 me-2 ms-2" for="data_nascita"><nobr>Città</nobr></label> 
                                <input name="citta" type="text" id="input_citta" class="form-control"                      value="<?php echo $utente['citta_utente']; ?>"
                                
                                
                                
                                
                                />
                            </div>
                        </div>
        



                        <div class="d-flex flex-row align-items-center mb-4">

                            <i class="fa-solid fa-city fa-lg me-3 fa-fw"></i>
                            <div class="form-outline flex-fill mb-0">               


                                <label class="text-black form-label p-0 m-0 me-md-3 me-2 ms-2" for="data_nascita"><nobr>CAP</nobr></label> 
    
                                <input name="cap" type="text" id="input_cap" 
                                class="form-control me-2 mb-2 mb-md-0" 
                                style="width: 100px;"

                                value="<?php echo $utente['cap_utente']; ?>"
                                    
                            
                            
                                />
                            </div>
                        </div>




                        <div class="d-flex flex-row align-items-center mb-4">

                        <i class="fa-solid fa-earth-americas fa-lg me-3 fa-fw"></i>
                            <div class="form-outline flex-fill mb-0">               


                                <label class="text-black form-label p-0 m-0 me-md-3 me-2 ms-2" for="data_nascita"><nobr>Nazionalità</nobr></label> 
    
                                <input name="nazionalita" type="text" id="input_nazione" class="form-control" 
                                value="<?php echo $utente['nazionalita_utente']; ?>"
                                
                                
                                />
                            </div>
                        </div>


        


            
            <!-- chiude row interna della form -->
            </div>


            <div class="d-flex justify-content-center w-100  mb-3 mb-lg-4 mt-3">
                <button type="submit" 
                        name="modifica_submit"
                        class="btn btn-lg w-100" style="background-color: #00bbaa; color: white;">Applica Modifiche
                </button>
            </div>

        </form>
        </div>
    </div>


</div>

<?php
    impedisci_submit_al_refresh();
?>