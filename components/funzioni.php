<?php

// VARIABILI GLOBALI
    // la variabile «connessione» globale esce fuori da qui
    $connessione = stabilisci_connessione('localhost', 'root', '', 'condexo_test1');



// SEZIONE UTILITY 

function stabilisci_connessione($host, $user, $pass, $database)
{
    $port = '3307';

    $db['db_host'] = $host . ':' . $port;
    $db['db_user'] = $user;
    $db['db_pass'] = $pass;
    $db['db_name'] = $database;
    

    // tramite questo stratagemma, di usare un array per conservare i dati della connessione
    // si può usare define per definire una costante runtime
    // che peraltro la funzione strtoupper si preoccupa di scrivere in maiuscolo
    // la scelta di assegnare i dati della connessione a delle costanti risponde a ragioni di sicurezza
    foreach($db as $key => $value)
    {
        define(strtoupper($key), $value);
    }
 
    $conn = mysqli_connect(DB_HOST, DB_USER, DB_PASS, DB_NAME);

    if($conn)
    {
        echo 
        "
            <script>
                console.log('%c connessione al database ' + '«{$database}»' + ' OK', 'color: #00BFFF;');    
            </script>
        ";
        
        return $conn;
    }
    else
    {
        echo 
        "
            <script>
                console.log('%c connessione al database ' + '[{$database}]' + ' FALLITA', 'color: #D04608;');    
            </script>
        ";
    };
};


function controlla_query($risultato, $contesto)
{
    global $connessione;

    if ($risultato)
    {
        echo
         "
            <script>
                console.info('%c Query ' + '«{$contesto}»' + ' OK', 'color: lime;');
                
            </script>
         ";
        return true;
    }
    else
    {
        die('Query failed > ' . mysqli_error($connessione));
        return false;
    }
}


function controlla_e_ritorna_query($risultato, $contesto)
{
    global $connessione;

    if ($risultato)
    {
        echo
         "
            <script>
                console.info('%c Query ' + '{$contesto}' + ' OK', 'color: lime;');
                
            </script>
         ";
        return $risultato;
    }
    else
    {
        die ('Query failed > ' . mysqli_error($connessione));
    }
}


function fix_input($dato)
{
  $dato = trim($dato);
  $dato = stripslashes($dato);
  $dato = htmlspecialchars($dato);
  return $dato;  
}


// FINE SEZIONE UTILITY



// SEZIONE GESTIONE UTENTE

function registra_utente()
{
    global $connessione;

    if (isset($_POST['registrazione_submit']))
    {
        echo
        "
           <script>
               console.info('%c dati form inviati e sottoposti a controllo', 'color: lime;');
           </script>
        ";

        $erroriInput = 0;

        // controlla input NOME
        if (empty($_POST['nome'])) 
        {
            $_SESSION['ErrNome'] = "Il campo Nome non può essere vuoto";
            $erroriInput++;
        } 
        elseif  (strlen($_POST['nome']) < 2)
        {
            $_SESSION['ErrNome'] = "Il Nome deve essere lungo almeno due caratteri";
            $erroriInput++;
        }
        elseif  (strlen($_POST['nome']) > 50)
        {
            $_SESSION['ErrNome'] = "Il Nome deve essere lungo non più di 50 caratteri";
            $erroriInput++;
        }
        else
        {
            $nome_utente = fix_input($_POST['nome']);
        }


        // controlla input COGNOME
        if (empty($_POST['cognome'])) 
        {
            $_SESSION['ErrCognome'] = "Il campo Cognome non può essere vuoto";
            $erroriInput++;
        } 
        elseif  (strlen($_POST['cognome']) < 2)
        {
            $_SESSION['ErrCognome'] = "Il Cognome deve essere lungo almeno due caratteri";
            $erroriInput++;
        }
        elseif  (strlen($_POST['cognome']) > 50)
        {
            $_SESSION['ErrCognome'] = "Il Cognome deve essere lungo non più di 50 caratteri";
            $erroriInput++;
        }
        else
        {
            $cognome_utente = fix_input($_POST['cognome']);
        }


        // controlla input EMAIL
        if (empty($_POST['email'])) 
        {
            $_SESSION['ErrEmail'] = "Il campo Email non può essere vuoto";
            $erroriInput++;
        } 
        elseif (!filter_var($_POST['email'], FILTER_VALIDATE_EMAIL)) 
        {
            $_SESSION['ErrEmail'] = 'Formato email non valido';
            $erroriInput++;
        }
        elseif  (strlen($_POST['email']) < 2)
        {
            $_SESSION['ErrEmail'] = "Lo indirizzo email deve essere lungo almeno due caratteri";
            $erroriInput++;
        }
        elseif  (strlen($_POST['email']) > 255)
        {
            $_SESSION['ErrEmail'] = "Lo indirizzo email deve essere lungo non più di 255 caratteri";
            $erroriInput++;
        }
        else
        {
            $email_utente = fix_input($_POST['email']);
        }


        // controlla input PASSWORD
        $conferma_password = $_POST['conferma_password'];
        $password = $_POST['password'];

        $number = preg_match('@[0-9]@', $password);
        $uppercase = preg_match('@[A-Z]@', $password);
        $lowercase = preg_match('@[a-z]@', $password);
        $specialChars = preg_match('@[^\w]@', $password);
         
        if(strlen($password) < 8 || !$number || !$uppercase || !$lowercase || !$specialChars) 
        {
            $_SESSION['ErrPassword'] 
            = 
            'La password deve essere lunga almeno 8 caratteri, deve contenere almeno un numero, un carattere maiuscolo, un carattere minuscolo e un carattere speciale';
            $erroriInput++;
        } 
        elseif ($password != $conferma_password)
        {
            $_SESSION['ErrPassword'] = 'La conferma password non coincide con la password';
            $erroriInput++;
        }
        else 
        {
            $password_utente = fix_input($_POST['password']);
        }

     
        $data_nascita_utente = fix_input($_POST['data_nascita']);
        $telefono_utente = fix_input($_POST['telefono']);
        $indirizzo_utente = fix_input($_POST['indirizzo']);
        $civico_utente = fix_input($_POST['civico']);
        $cap_utente = fix_input($_POST['cap']);
        $citta_utente = fix_input($_POST['citta']);
        $nazionalita_utente = fix_input($_POST['nazionalita']);
        

        // visualizza nella console JS l'esito del prelievo dei dati dalla form
        echo
        "
           <script>
               console.info('nome utente > {$nome_utente}');
               console.info('cognome utente > {$cognome_utente}');
               console.info('email utente > {$email_utente}');
               console.info('password utente > {$password_utente}');
               console.info('password utente > {$data_nascita_utente}');
               console.info('telefono utente > {$telefono_utente}');
               console.info('indirizzo utente > {$indirizzo_utente}');
               console.info('civico utente > {$civico_utente}');
               console.info('cap utente > {$cap_utente}');
               console.info('città utente > {$citta_utente}');
               console.info('nazionalità utente > {$nazionalita_utente}');
    
           </script>
        ";

        // controlla che tutti i campi della form siano stati validati
        // se la validazione passa, allora effettua la query
        if ($erroriInput == 0)
        {
            $query =
            "
                INSERT INTO utenti
                (
                    nome_utente, cognome_utente, email_utente, password_utente,
                    data_nascita_utente,
                    telefono_utente, indirizzo_utente, 
                    civico_utente,
                    cap_utente, citta_utente, 
                    nazionalita_utente
                )
                VALUES
                (
                    '{$nome_utente}', '{$cognome_utente}', '{$email_utente}', '{$password_utente}',
                    '{$data_nascita_utente}',
                    '{$telefono_utente}', '{$indirizzo_utente}', 
                    '{$civico_utente}',
                    '{$cap_utente}', '{$citta_utente}', 
                    '{$nazionalita_utente}'
                )
            
            ";
    
            $query_registra_utente = mysqli_query($connessione, $query);
    
            $contesto = 'registrazione utente';
            if(controlla_query($query_registra_utente, $contesto))
            {
                unset($_SESSION['ErrNome']);
                unset($_SESSION['ErrCognome']);
                unset($_SESSION['ErrEmail']);
                unset($_SESSION['ErrPassword']);
                
                echo
                "
                    <script>
                        animazione_registrazione();

                        setTimeout (refresh_page, 5000);

                        function refresh_page()
                        {
                            window.location.href = 'http://localhost/condexo_task1/index.php';
                        }

                    </script>
                ";
                
                
            };
        }
        else
        {
            echo
            "
                <script>
                    console.error('i dati della form registrazione non sono corretti e il record non verrà salvato');
                    console.warn('{$_SESSION['ErrNome']}');
                    console.warn('{$_SESSION['ErrCognome']}');
                    console.warn('{$_SESSION['ErrEmail']}');
                    console.warn('{$_SESSION['ErrPassword']}');

                    window.location.href = 'http://localhost/condexo_task1/index.php';
                </script>
            ";
        }
    }
    else
    {
        echo
        "
           <script>
               console.info('%c dati form NON inviati, forse form duplicato?');
           </script>
        ";
    }
}


function leggi_tutti_utenti()
{
    global $connessione;

    $query = "SELECT * FROM utenti ";
    $query_seleziona_tutti_utenti = mysqli_query($connessione, $query);

    $contesto = 'leggi tutti gli utenti';
    if (controlla_query($query_seleziona_tutti_utenti, $contesto))
    {
        return $query_seleziona_tutti_utenti;
    }

}


function mostra_utenti_tabella($utenti)
{
    while ($row = mysqli_fetch_assoc($utenti))
    {
        $id = $row['id_utente'];
        $nome = $row['nome_utente'];
        $cognome = $row['cognome_utente'];
        $email = $row['email_utente'];
        $password = $row['password_utente'];
        $data_nascita = date_create($row['data_nascita_utente']);
        
        $data_nascita_formato_italiano = 
        date_format($data_nascita, 'd/m/Y');

        $telefono = $row['telefono_utente'];
        $indirizzo = $row['indirizzo_utente'];
        $civico = $row['civico_utente'];
        $cap = $row['cap_utente'];
        $citta = $row['citta_utente'];
        $nazionalita = $row['nazionalita_utente'];
        $is_admin = $row['is_admin'];

        if ($is_admin)
        {
            $status = 'admin';
        }
        else
        {
            $status = 'base';
        }
        
        echo  
        "
        <tr>
            <th scope='row'>{$id}</th>
            <td><nobr>{$nome}</nobr></td>
            <td><nobr>{$cognome}</nobr></td>
            <td>{$email}</td>
            <td>{$password}</td>
            <td><nobr>{$data_nascita_formato_italiano}</nobr></td>
            <td>{$telefono}</td>
            <td><nobr>{$indirizzo}</nobr></td>
            <td>{$civico}</td>
            <td>{$cap}</td>
            <td><nobr>{$citta}</nobr></td>
            <td>{$nazionalita}</td>
            <td>{$status}</td>
            <td>
                <a 
                    class='operations-icon'
                    href='admin.php?content=dettaglio&u_id=$id'>
                    <i class='fa-solid fa-eye'></i>
                </a>
                <a 
                    class='operations-icon'
                    href='admin.php?content=edit&u_id=$id'>
                    <i class='fa-solid fa-pen-to-square'></i>
                </a>
                <a 
                    class='operations-icon'
                    href='admin.php?delete&u_id=$id'>
                    <i class='fa-solid fa-eraser'></i>
                </a>

            </td>
        </tr>
        ";
    }
}


function impedisci_submit_al_refresh()
{
    echo
    "<script>
        if (window.history.replaceState) {
        window.history.replaceState(null, null, window.location.href);
      }
    </script>";
}


function dettaglio_utente()
{
    global $connessione;

    if (isset($_GET['content']))
    {
        $id = $_GET['u_id'];       
        $utente = leggi_utente($id);
        if ($utente)
        {
            return $utente;
        }
    }
}


function leggi_utente($id)
{
    global $connessione;

    $query = 
    "
        SELECT * FROM utenti WHERE id_utente = $id;
    ";
    
    $query_seleziona_utente = mysqli_query($connessione, $query);
    
    $contesto = 'leggi singolo utente';
    if (controlla_query($query_seleziona_utente, $contesto))
    {
        $utente = mysqli_fetch_assoc($query_seleziona_utente);
        return $utente;
    }

}



function modifica_utente($id)
{

    global $connessione;


    if (isset($_POST['modifica_submit']))
    {
        echo
        "
           <script>
               console.info('dati form modifica inoltrati');             
           </script>
        ";

        $nome_utente = $_POST['nome'];
        $cognome_utente = $_POST['cognome'];
        $email_utente = $_POST['email'];
        $password_utente = $_POST['password'];
        $data_nascita_utente = $_POST['data_nascita'];
        $telefono_utente = $_POST['telefono'];
        $indirizzo_utente = $_POST['indirizzo'];
        $civico_utente = $_POST['civico'];
        $cap_utente = $_POST['cap'];
        $citta_utente = $_POST['citta'];
        $nazionalita_utente = $_POST['nazionalita'];
       

        // mostra in console JS l'esito del prelievo dei dati per l'edit utente dalla form
        echo
        "
           <script>
               console.info('DATI MODIFICA');
               console.info('id > {$id}');
               console.info('nome utente > {$nome_utente}');
               console.info('cognome utente > {$cognome_utente}');
               console.info('email utente > {$email_utente}');
               console.info('password utente > {$password_utente}');
               console.info('data di nascita > {$data_nascita_utente}');
               console.info('telefono utente > {$telefono_utente}');
               console.info('indirizzo utente > {$indirizzo_utente}');
               console.info('civico utente > {$civico_utente}');
               console.info('cap utente > {$cap_utente}');
               console.info('città utente > {$citta_utente}');
               console.info('nazionalità utente > {$nazionalita_utente}');
           </script>
        ";


        $query =
        "
        UPDATE utenti
        SET
            nome_utente = '{$nome_utente}',
            cognome_utente = '{$cognome_utente}',
            email_utente = '{$email_utente}',
            password_utente = '{$password_utente}',
            data_nascita_utente = '{$data_nascita_utente}',
            telefono_utente = '{$telefono_utente}',
            indirizzo_utente = '{$indirizzo_utente}',
            civico_utente = '{$civico_utente}',
            cap_utente = '{$cap_utente}',
            citta_utente = '{$citta_utente}',
            nazionalita_utente = '{$nazionalita_utente}'
        WHERE id_utente = {$id};
        ";

        $query_modifica_utente = mysqli_query($connessione, $query);

        $contesto = 'modifica singolo utente';
        if(controlla_query($query_modifica_utente, $contesto))
        {
            echo
            "
                <script>
                   alert('utente modificato');
                </script>
            "; 
        }

    }
}




function elimina_utente()
{
    global $connessione;

    if (isset($_GET['delete']))
    {
        $id = $_GET['u_id'];

        $query =
        "
            DELETE FROM utenti
            WHERE id_utente = {$id};
        ";

        $query_elimina_utente = mysqli_query($connessione, $query);

        $contesto = 'elimina utente';

        if(controlla_query($query_elimina_utente, $contesto))
        {
            header('Location: admin.php?content=utenti');
        }
    }
}


function seleziona_utenti_ricerca($utente_cercato)
{
    global $connessione;

    echo
    "
        <script>
            console.log('utente cercato > ' +  '{$utente_cercato}');
        </script>
    ";

    $query = 
    "
        SELECT * FROM utenti
        WHERE 
            cognome_utente LIKE '%$utente_cercato%' 
        OR
            nome_utente LIKE '%$utente_cercato%';
    ";

    $query_cerca_utente = mysqli_query($connessione, $query);

    $contesto = 'cerca utente';

    if(controlla_query($query_cerca_utente, $contesto))
    {
        return $query_cerca_utente;
    }
}


function leggi_utenti()
{
    if (!isset($_POST['search_submit']))
    {
        $utenti = leggi_tutti_utenti();
    }
    else
    {
        $utente_cercato = $_POST['utente_cercato'];
        $utenti = seleziona_utenti_ricerca($utente_cercato);
    }

    return $utenti;
}


?>