<!-- box ricerca record  utenti esterno alla tabella -->
<div 
    class="d-flex justify-content-end position-absolute pb-4 w-100">
    <div 
        id="box-ricerca-esterno"
        class="shadow-sm  py-3
                d-flex justify-content-end align-items-center w-100">
        <form 
            class="d-flex justify-content-end align-items-center m-0"
            method="POST"
            action="admin.php?content=utenti">
                                
            <span class="text-white me-1">Ricerca</span>
            <input 
                name="utente_cercato"
                class="rounded">
            <button 
                name="search_submit"
                class="mx-1 rounded"
                type="submit"
                style="height: 30px">
                <i class="fa-solid fa-magnifying-glass"></i>    
            </button>
        </form>
    </div>
</div>


<!-- tabella -->
<div class="text-black px-1 pe-md-2 ps-3 pt-5" 
        style="color: black;  overflow-y:scroll; overflow-x: scroll;
                max-height:100%; width: 100%;">

    <!-- box ricerca record  utenti interna alla tabella -->
    <div 
        class="d-flex justify-content-center justify-content-md-end sticky-top pb-4  ">

        <div 
            id="box-ricerca-interno"
            class="rounded shadow-sm px-2 py-3 d-flex justify-content-center"
            style="width: 335px">
            
            <form 
                class="d-flex justify-content-end align-items-center m-0"
                method="POST"
                action="admin.php?content=utenti">
                    
                <span class="text-white me-1">Ricerca</span>
                <input class="rounded"
                        name="utente_cercato">
                <button 
                    class="mx-1 rounded"
                    type="submit"
                    name="search_submit"
                    style="height: 30px">
                    <i class="fa-solid fa-magnifying-glass"></i>    
                </button>
            </form>
        </div>
    </div>



    <table class="table table-striped table-hover"
            style="font-size: 0.9rem;">
    
        <h3>Utenti registrati</h3>
        <thead>
            <tr>
            <th scope="col">ID</th>
            <th scope="col">Nome</th>
            <th scope="col">Cognome</th>
            <th scope="col">Email</th>
            <th scope="col">Password</th>
            <th scope="col"><nobr>Data di nascita</nobr></th>
            <th scope="col">Telefono</th>
            <th scope="col">Indirizzo</th>
            <th scope="col">N.Civ.</th>
            <th scope="col">CAP</th>
            <th scope="col">Città</th>
            <th scope="col">Nazionalità</th>
            <th scope="col">Status</th>
            <th scope="col">Operazioni</th>
            </tr>
        </thead>
        <tbody>

        <?php 
            $utenti = leggi_utenti();
            mostra_utenti_tabella($utenti);
        ?>
     
        </tbody>
    </table>

</div>