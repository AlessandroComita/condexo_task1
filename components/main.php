<body>

<div class="container-fluid h-100 ">
    <div class="row  h-100">
        <div class="col-12  h-100 p-0 d-flex">

            <?php include "components/sidebar.php" ?>

            <?php
                if (isset($_GET['content']))
                {
                    $content = $_GET['content'];

                    switch ($content)
                    {
                        case 'home':
                                // sezione  benvenuto
                                include "components/welcome.php";
                                break;
                        case 'utenti':
                                // sezione tabella utenti registrati
                                include "components/tabella-utenti.php";
                                break;
                        case 'dashboard':
                                include "components/dashboard.php";
                                break;
                        case 'edit':
                            include "components/edit.php";
                            break;
                        case 'dettaglio':
                            include "components/dettaglio.php";
                            break;
                        default:
                            include "components/welcome.php";
                    }
                }
                else
                {
                    include "components/welcome.php";           
                }
            ?>
                  
        </div>

    </div>
</div>


    
</body>
