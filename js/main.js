// cattura elemento sidebar - se presente
if(document.getElementById('admin_sidebar'))
{
    e_sidebar = document.getElementById('admin_sidebar');
    console.log(e_sidebar);
    e_admin_button = document.getElementById('admin_button');
    console.log(e_admin_button);
    e_button_sidebar_toggle = document.getElementById('button_sidebar_toggle');
    console.log(e_button_sidebar_toggle);

    e_admin_button.addEventListener('click', toggle_sidebar);
    e_button_sidebar_toggle.addEventListener('click', entra_sidebar);
}

e_box_messaggio_registrazione = document.getElementById('box-messaggio-registrazione');
console.log(e_box_messaggio_registrazione);

e_wrap_messaggio_registrazione = document.getElementById('wrap-messaggio-registrazione');
console.log(e_wrap_messaggio_registrazione);


function toggle_sidebar()
{
    e_sidebar.classList.remove('compari_lato_sinistro');
    e_sidebar.classList.add('scompari_lato_sinistro');
}

function entra_sidebar()
{
    e_sidebar.classList.remove('scompari_lato_sinistro');
    e_sidebar.classList.add('compari_lato_sinistro');
}

console.log('mostra messaggio animazione_registrazione BOTTOM', mostra_messaggio_registrazione);

if(mostra_messaggio_registrazione)
{
    e_wrap_messaggio_registrazione.classList.add('compari-tasto');
    setTimeout(nascondi_tasto_registrazione, 5000);
    mostra_messaggio_registrazione = false;
}

function nascondi_tasto_registrazione()
{
    e_wrap_messaggio_registrazione.classList.remove('compari-tasto');
}